import org.junit.Test

class CategorizeNewMemberTest {
    @Test
    void "Fixed Tests" () {
        assert CategorizeNewMember.openOrSenior(convert([[45, 12],[55,21],[19, -2],[104, 20]])) == ['Open', 'Senior', 'Open', 'Senior']
        assert CategorizeNewMember.openOrSenior(convert([[3, 12],[55,1],[91, -2],[54, 23]]))    == ['Open', 'Open', 'Open', 'Open']
        assert CategorizeNewMember.openOrSenior(convert([[59, 12],[55,-1],[12, -2],[12, 12]]))  == ['Senior', 'Open', 'Open', 'Open']
        assert CategorizeNewMember.openOrSenior(convert([[74, 10],[55, 6],[12, -2],[68, 7]]))   == ['Senior', 'Open', 'Open', 'Open']
        assert CategorizeNewMember.openOrSenior(convert([[16, 23],[56, 2],[56,  8],[54, 6]]))   == ['Open', 'Open', 'Senior', 'Open']
    }

    @Test
    void "Random Tests" () {
        def ri
        (1..100).each { _ ->
            ri = randomInput()
            // println "Testing for $ri"
            assert CategorizeNewMember.openOrSenior(ri) == solution(ri)
        }
    }

    private def random = new Random()

    private def randomInput () {
        def arrLen = 3 + random.nextInt(5)
        (0..arrLen).collect { _ ->
            new Tuple(
                    random.nextInt(40) + random.nextInt(40) + 10,
                    random.nextInt(29) - 2
            )
        }
    }

    private static def convert(l) {
        l.collect { t -> new Tuple(t[0], t[1]) }
    }

    // Kata author's solution
    private static def solution(data) {
        data.collect { t -> t[0] > 54 && t[1] > 7 ? "Senior" : "Open" }
    }
}
