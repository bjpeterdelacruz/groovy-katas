import org.junit.Test

import java.time.LocalDate

class MostFrequentWeekdaysTest {
    @Test
    void fixedTests() {
        assert MostFrequentWeekdays.mostFrequentDays(1770) == ["Monday"]
        assert MostFrequentWeekdays.mostFrequentDays(1785) == ["Saturday"]
        assert MostFrequentWeekdays.mostFrequentDays(1901) == ["Tuesday"]
        assert MostFrequentWeekdays.mostFrequentDays(2135) == ["Saturday"]
        assert MostFrequentWeekdays.mostFrequentDays(3043) == ["Sunday"]
        assert MostFrequentWeekdays.mostFrequentDays(2001) == ["Monday"]
        assert MostFrequentWeekdays.mostFrequentDays(3150) == ["Sunday"]
        assert MostFrequentWeekdays.mostFrequentDays(3230) == ["Tuesday"]
        assert MostFrequentWeekdays.mostFrequentDays(2016) == ["Friday", "Saturday"]
        assert MostFrequentWeekdays.mostFrequentDays(1986) == ["Wednesday"]
        assert MostFrequentWeekdays.mostFrequentDays(3361) == ["Thursday"]
        assert MostFrequentWeekdays.mostFrequentDays(1910) == ["Saturday"]
        assert MostFrequentWeekdays.mostFrequentDays(1968) == ["Monday", "Tuesday"]
        assert MostFrequentWeekdays.mostFrequentDays(1794) == ["Wednesday"]
        assert MostFrequentWeekdays.mostFrequentDays(2000) == ["Saturday", "Sunday"]
        assert MostFrequentWeekdays.mostFrequentDays(1868) == ["Wednesday", "Thursday"]
    }

    @Test
    void randomTests() {
        def random = new Random()
        (1..100).forEach { _ ->
            def randomInput = random.nextInt(2418) + 1583
            assert MostFrequentWeekdays.mostFrequentDays(randomInput) == solution(randomInput)
        }
    }

    // Kata author's solution
    private static String[] solution (year) {
        def weekDay = { day -> day.getDayOfWeek().toString().toLowerCase().capitalize() }

        def firstDayOfYear = LocalDate.of(year, 1, 1)
        def firstDay = weekDay(firstDayOfYear)

        if (!firstDayOfYear.isLeapYear()) {
            return [firstDay]
        }

        def secondDay = weekDay(firstDayOfYear.plusDays(1))

        return (secondDay == "Monday") ? [secondDay, firstDay] : [firstDay, secondDay]
    }
}
