import org.junit.Test

class DigitsAverageTest {
    @Test
    void "Basic tests"() {
        assert DigitsAverage.digitsAverage(246) == 4
        assert DigitsAverage.digitsAverage(89) == 9
        assert DigitsAverage.digitsAverage(2) == 2
        assert DigitsAverage.digitsAverage(245) == 4
        assert DigitsAverage.digitsAverage(345) == 5
        assert DigitsAverage.digitsAverage(346) == 5
    }

    // Kata author's solution
    static int sol(i) {
        i = i.toString().split("(?!^)").collect{it as int}
        while (i.size() != 1){
            def r = []
            for (int k = 0; k < i.size()-1; k++)
                r[k] = Math.ceil((i[k] + i[k+1])/2)
            i = r
        }
        return i[0]
    }

    @Test
    void "Random tests"() {
        def r = new Random()
        50.times {
            int num = r.nextInt(99999)
            // println "Testing for $num"
            assert DigitsAverage.digitsAverage(num) == sol(num)
        }
    }
}
