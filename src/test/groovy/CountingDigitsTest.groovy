import org.junit.Test

class CountingDigitsTest {
    @Test
    void "Fixed Test"() {
        assert CountingDigits.countSpecDigits([1, 1, 2 ,3 ,1 ,2 ,3 ,4], [1, 3]) == [[1, 3], [3, 2]]
        assert CountingDigits.countSpecDigits([-18, -31, 81, -19, 111, -888], [1, 8, 4]) == [[1, 7], [8, 5], [4, 0]]
        assert CountingDigits.countSpecDigits([-77, -65, 56, -79, 6666, 222], [1, 8, 4]) == [[1, 0], [8, 0], [4, 0]]
        assert CountingDigits.countSpecDigits([], [1, 8, 4]) == [[1, 0], [8, 0], [4, 0]]
    }

    @Test
    void "Random Tests"() {
        randomTests(50, 100, 100, 7)
        randomTests(30, 1000, 50, 8)
        randomTests(20, 10000, 50, 9)
    }

    private def randomTests(ntests, rng, len, digLen) {
        def rnd = new Random()
        rnd.with {
            (1..ntests).each {
                def randomIntegers = (1..rng).collect {
                    def sign = 1
                    if(nextInt(rng) % 2 == 0) {
                        sign = -1
                    }
                    nextInt(rng) * sign
                }

                def randomDigits = (0..9).toList()
                Collections.shuffle(randomDigits)
                randomDigits = randomDigits.take(digLen)
                // println "Testing for ($randomIntegers, $randomDigits)"
                assert CountingDigits.countSpecDigits(randomIntegers, randomDigits) == solution(randomIntegers, randomDigits)
            }
        }
    }

    // Kata author's solution
    private static def solution(integers, digits) {
        def digitList = integers.join("").split("")
        digits.collect { [it, digitList.count(it.toString())] }
    }
}
