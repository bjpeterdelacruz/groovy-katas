import org.junit.Test

class SimpleTranspositionTest {
    @Test
    void "Fixed Tests"() {
        assert SimpleTransposition.simpleTransposition("Sample text") == "Sml etapetx"
        assert SimpleTransposition.simpleTransposition("Simple transposition") == "Sml rnpstoipetasoiin"
        assert SimpleTransposition.simpleTransposition("All that glitters is not gold") == "Alta ltesi o odl htgitr sntgl"
        assert SimpleTransposition.simpleTransposition("The better part of valor is discretion") == "Tebte ato ao sdsrtoh etrpr fvlri icein"
        assert SimpleTransposition.simpleTransposition("Conscience does make cowards of us all") == "Cncec osmk oad fu losinede aecwrso sal"
        assert SimpleTransposition.simpleTransposition("Imagination is more important than knowledge") == "Iaiaini oeipratta nwegmgnto smr motn hnkolde"
    }

    @Test
    void "Random Test"() {
        (1..100).each { _ ->
            def input = randomInput()
            assert SimpleTransposition.simpleTransposition(input) == solution(input)
        }
    }

    private static def randomInput() {
        def random = new Random()
        def alphabet = (('a'..'z') + ('A'..'Z')).join()
        def len = alphabet.size()
        random.with {
            (1..(nextInt(5)+1)).collect {
                (1..(nextInt(50)+1)).collect { alphabet[nextInt(len)] }.join()
            }.join(" ")
        }
    }

    // Kata author's solution
    private static def solution(text) {
        def firstRow = []
        def secondRow = []
        def toggle = true
        text.each { c ->
            (toggle ? firstRow : secondRow) << c
            toggle = !toggle
        }
        "${firstRow.join()}${secondRow.join()}"
    }
}
