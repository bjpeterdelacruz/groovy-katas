import org.junit.Test

class ElevatorDistanceTest {
    @Test
    void "STATIC TESTS"() {
        assert ElevatorDistance.elevatorDistance([5,2,8]) == 9
        assert ElevatorDistance.elevatorDistance([1,2,3]) == 2
        assert ElevatorDistance.elevatorDistance([7,1,7,1]) == 18
    }

    // Kata author's solution
    static int elD(a) {
        int dist = 0;
        for (int i = 0; i < a.size() - 1; i++)
            dist += Math.abs(a[i] - a[i+1])
        dist
    }

    @Test
    void "RANDOM TESTS"() {
        def r = new Random()
        21.times {
            def aran = []
            def alen = r.nextInt(18)+2
            alen.times{
                aran.push(r.nextInt(30))
            }
            // println "Floor array is $aran"
            assert ElevatorDistance.elevatorDistance(aran) == elD(aran)
        }
    }
}
