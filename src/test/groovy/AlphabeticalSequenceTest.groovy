import org.junit.Test

class AlphabeticalSequenceTest {
    @Test
    void "Fixed Tests" () {
        assert AlphabeticalSequence.alphaSeq("BfcFA") == "A,Bb,Ccc,Ffffff,Ffffff"
        assert AlphabeticalSequence.alphaSeq("ZpglnRxqenU") == "Eeeee,Ggggggg,Llllllllllll,Nnnnnnnnnnnnnn,Nnnnnnnnnnnnnn,Pppppppppppppppp,Qqqqqqqqqqqqqqqqq,Rrrrrrrrrrrrrrrrrr,Uuuuuuuuuuuuuuuuuuuuu,Xxxxxxxxxxxxxxxxxxxxxxxx,Zzzzzzzzzzzzzzzzzzzzzzzzzz"
        assert AlphabeticalSequence.alphaSeq("NyffsGeyylB") == "Bb,Eeeee,Ffffff,Ffffff,Ggggggg,Llllllllllll,Nnnnnnnnnnnnnn,Sssssssssssssssssss,Yyyyyyyyyyyyyyyyyyyyyyyyy,Yyyyyyyyyyyyyyyyyyyyyyyyy,Yyyyyyyyyyyyyyyyyyyyyyyyy"
        assert AlphabeticalSequence.alphaSeq("MjtkuBovqrU") == "Bb,Jjjjjjjjjj,Kkkkkkkkkkk,Mmmmmmmmmmmmm,Ooooooooooooooo,Qqqqqqqqqqqqqqqqq,Rrrrrrrrrrrrrrrrrr,Tttttttttttttttttttt,Uuuuuuuuuuuuuuuuuuuuu,Uuuuuuuuuuuuuuuuuuuuu,Vvvvvvvvvvvvvvvvvvvvvv"
        assert AlphabeticalSequence.alphaSeq("EvidjUnokmM") == "Dddd,Eeeee,Iiiiiiiii,Jjjjjjjjjj,Kkkkkkkkkkk,Mmmmmmmmmmmmm,Mmmmmmmmmmmmm,Nnnnnnnnnnnnnn,Ooooooooooooooo,Uuuuuuuuuuuuuuuuuuuuu,Vvvvvvvvvvvvvvvvvvvvvv"
        assert AlphabeticalSequence.alphaSeq("HbideVbxncC") == "Bb,Bb,Ccc,Ccc,Dddd,Eeeee,Hhhhhhhh,Iiiiiiiii,Nnnnnnnnnnnnnn,Vvvvvvvvvvvvvvvvvvvvvv,Xxxxxxxxxxxxxxxxxxxxxxxx"
    }

    @Test
    void "Random Tests" () {
        def random = new Random()
        random.with {
            (1..100).each {
                def randomInput = (1..(nextInt(19)+1)).collect { [('a'..'z'), ('A'..'Z')][nextInt(100)%2][nextInt(25)] }.join()
                def expected = solution(randomInput)
                // println "Testing for $randomInput"
                assert AlphabeticalSequence.alphaSeq(randomInput) == expected
            }
        }
    }

    // Kata author's solution
    private static def solution(string) {
        string.toLowerCase().toList().sort().collect { c -> c.toUpperCase() + c * ((int) c - 97) }.join(",")
    }
}
