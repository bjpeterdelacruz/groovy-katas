import org.junit.Test

class ProductOfArrayItemsTest {
    @Test
    void "Fixed Tests"() {
        assert ProductOfArrayItems.product([5, 4, 1, 3, 9]) ==  540
        assert ProductOfArrayItems.product([-2, 6, 7, 8]) ==  -672
        assert ProductOfArrayItems.product([10]) ==  10
        assert ProductOfArrayItems.product([0, 2, 9, 7]) == 0
        assert ProductOfArrayItems.product(null) == null
        assert ProductOfArrayItems.product([]) == null
    }

    @Test
    void "Random Tests"() {
        (1..100).each {
            def ri = randomInput()
            assert ProductOfArrayItems.product(ri) == solution(ri)
        }
    }

    private static def randomInput() {
        def random = new Random();
        switch(random.nextInt(20) % 5) {
            case 4: null;break
            case 5: [];break
            default: (1..(random.nextInt(200)+1)).collect {random.nextInt(1000)}
        }
    }

    // Kata author's solution
    private static def solution(numbers) {
        numbers == null || numbers.isEmpty() ? null : numbers.inject(1) { acc, elem -> acc * elem }
    }
}
