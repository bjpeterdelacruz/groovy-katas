import org.junit.Test

class RepeaterTest {
    @Test
    void "Basic Tests" () {
        assert Repeater.repeater('a', 5) == 'aaaaa'
        assert Repeater.repeater('Na', 16) == 'NaNaNaNaNaNaNaNaNaNaNaNaNaNaNaNa'
        assert Repeater.repeater('Wub ', 6) == 'Wub Wub Wub Wub Wub Wub '
    }

    @Test
    void "More Tests" () {
        assert Repeater.repeater('la', 12) == solution('la', 12)
        assert Repeater.repeater('dance\n', 3) == solution('dance\n', 3)
        assert Repeater.repeater('A stopped clock is right twice a day. ', 2) == solution('A stopped clock is right twice a day. ', 2)
        assert Repeater.repeater('nine', 9) == solution('nine', 9)
    }

    @Test
    void "Random Tests" () {
        def random = new Random()
        def randomWords = ["This", "is", "an", "array", "of", "random", "strings", "that", "might", "show", "up", "in", "some", "test"]
        def length = randomWords.size() - 1
        random.with {
            (1..100).each {
                def randomWord = randomWords[nextInt(length)]
                def repeat = nextInt(10) + 1
                assert Repeater.repeater(randomWord, repeat) == solution(randomWord, repeat)
            }
        }
    }

    // Kata author's solution
    private static def solution(string, n) {
        string * n
    }
}
