import org.junit.Test

import java.time.DayOfWeek
import java.time.LocalDate

class UnluckyDaysTest {
    @Test
    void fixedTets() {
        assert UnluckyDays.unluckyDays(2015) == 3
        assert UnluckyDays.unluckyDays(1986) == 1
        assert UnluckyDays.unluckyDays(1634) == 2
        assert UnluckyDays.unluckyDays(2873) == 2
    }

    @Test
    void randomTests() {
        def random = new Random()
        (1..100).forEach { _ ->
            def randomInput = random.nextInt(10000) % 2000 + 1583
            assert UnluckyDays.unluckyDays(randomInput) == solution(randomInput)
        }
    }

    // Kata author's solution
    private static solution(Integer year) {
        def result = 0
        (1..12).forEach { month ->
            if (LocalDate.of(year, month, 13).getDayOfWeek() == DayOfWeek.FRIDAY) {
                result += 1
            }
        }
        result
    }
}
