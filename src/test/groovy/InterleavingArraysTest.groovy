import org.junit.Test

class InterleavingArraysTest {
    @Test
    void "Fixed Tests"() {
        assert InterleavingArrays.interleave([1, 2, 3], ["c", "d", "e"]) == [1, "c", 2, "d", 3, "e"]
        assert InterleavingArrays.interleave([1, 2, 3], [4, 5]) == [1, 4, 2, 5, 3, null]
        assert InterleavingArrays.interleave([1, 2, 3], [4, 5, 6], [7, 8, 9]) == [1, 4, 7, 2, 5, 8, 3, 6, 9]
        assert InterleavingArrays.interleave([1, 2], [4, 5, 6], [7, 8, 9]) == [1, 4, 7, 2, 5, 8, null, 6, 9]
        assert InterleavingArrays.interleave() == []
    }

    @Test
    void "Random Test"() {
        def rnd = new Random()
        def source = (0..9) + ('a'..'z') + ('A'..'Z')
        def maxInd = source.size() - 1

        def randomInput = { ->
            (0..rnd.nextInt(10)).collect { _ -> (0..rnd.nextInt(10)).collect { source[rnd.nextInt(maxInd)] } }
        }

        def input
        def expected

        (1..100).each { _ ->
            input = randomInput()
            expected = solution(*input)
            assert InterleavingArrays.interleave(*input) == expected
        }
    }

    // Kata author's solution
    private static def solution(... args) {
        (0..<(args.collect { it.size() }.max() ?: 0)).collect {i -> args.collect { l -> l[i] }}.flatten()
    }
}
