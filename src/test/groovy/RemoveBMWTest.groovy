import org.junit.Test

class RemoveBMWTest {
    @Test
    void "Test for a string that contains only letters b, m and w"() {
        assert RemoveBMW.removeBMW("bbmmwwWWBBMM") == '', "should be empty"
    }

    static void assert_error(inp) {
        try {
            RemoveBMW.removeBMW(inp)
            throw new IllegalArgumentException("Invalid inputs must throw exception")
        } catch (IllegalArgumentException ex) {
            if (ex.message != "This program only works for text.")
                throw new IllegalArgumentException("Message for IllegalArgumentException should be \"This program only works for text.\"")
        } catch (ex) {
            throw new IllegalArgumentException("Invalid inputs must throw IllegalArgumentException, not " + ex.class)
        }
    }

    @Test
    void "Test for invalid inputs"() {
        for (inp in [2, [], {}, ['oops']]) assert_error(inp)
    }

    static final r = new Random()

    static String rndW() {
        def s = new StringBuilder()
        for (i in 0..r.nextInt(100)) s.append(alpha[r.nextInt(52)])
        return s.toString()
    }

    static final alpha = ('A'..'Z') + ('a'..'z')
    @Test
    void "Test for random inputs"() {
        for (def i = 0; i < 100; i++) {
            if (r.nextInt(10) > 0) {
                def s = rndW()
                // Kata author's solution
                def exp = s.replaceAll(/[bmwBMW]/, '')
                assert exp == RemoveBMW.removeBMW(s)
            } else {
                def x = r.nextInt(3)
                def inp = (x == 0) ? r.nextInt(20) - 10 :
                          (x == 1) ? [r.nextInt(20) - 10, r.nextInt(20) - 10, r.nextInt(20) - 10] :
                                     [rndW(), rndW(), rndW()]
                assert_error(inp)
            }
        }
    }
}
