class RemoveBMW {
    static removeBMW(Object str) {
        if (!(str instanceof String)) {
            throw new IllegalArgumentException("This program only works for text.")
        }
        str.toString().replaceAll("[BMWbmw]+", "")
    }
}
