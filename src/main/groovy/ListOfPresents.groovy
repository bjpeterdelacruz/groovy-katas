class ListOfPresents {
    static int howManyGifts(int maxBudget, List<Integer> gifts) {
        def sortedGifts = gifts.sort()
        def numGifts = 0
        def total = 0
        for (Integer gift : sortedGifts) {
            if (gift + total <= maxBudget) {
                total += gift
                numGifts++
            }
            else {
                break
            }
        }
        numGifts
    }
}
