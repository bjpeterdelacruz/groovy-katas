class CategorizeNewMember {
    static def openOrSenior(List<Tuple> data) {
        def categories = []
        final def seniorAge = 55
        final def handicap = 7
        for (Tuple tuple : data) {
            if (tuple[0] >= seniorAge && tuple[1] > handicap) {
                categories.add("Senior")
            }
            else {
                categories.add("Open")
            }
        }
        categories
    }
}
