class InterleavingArrays {
    static def interleave(... args) {
        def longest = -1
        def lists = (args as List<ArrayList>)
        for (ArrayList list : lists) {
            if (list.size() > longest) {
                longest = list.size()
            }
        }
        def result = new ArrayList()
        for (def idx = 0; idx < longest; idx++) {
            for (ArrayList list : lists) {
                if (idx < list.size()) {
                    result.add(list[idx])
                } else {
                    result.add(null);
                }
            }
        }
        result
    }
}
