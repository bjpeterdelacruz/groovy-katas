import java.time.DayOfWeek
import java.time.LocalDate
import java.time.temporal.TemporalAdjusters

class UnluckyDays {
    static int unluckyDays(int year) {
        LocalDate date = LocalDate.of(year, 1, 1)
        int count = 0
        while (date.getYear() == year) {
            date = date.with(TemporalAdjusters.next(DayOfWeek.FRIDAY))
            if (date.getDayOfMonth() == 13) {
                count++
            }
        }
        count
    }
}
