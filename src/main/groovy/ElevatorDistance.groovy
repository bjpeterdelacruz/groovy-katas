class ElevatorDistance {

    static int elevatorDistance(List<Integer> floors) {
        def sum = 0
        for (def idx = 1; idx < floors.size(); idx++) {
            if (floors[idx - 1] != floors[idx]) {
                sum += Math.abs(floors[idx - 1] - floors[idx])
            }
        }
        sum
    }

}
