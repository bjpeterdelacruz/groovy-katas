import java.time.DayOfWeek
import java.time.LocalDate
import java.time.temporal.TemporalAdjusters

class MostFrequentWeekdays {
    static mostFrequentDays(int year) {
        def days = [DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY,
                    DayOfWeek.SATURDAY, DayOfWeek.SUNDAY]
        def counts = [:]
        days.each { counts.put(it, 0) }
        def maximum = 0
        for (DayOfWeek day : days) {
            LocalDate date = LocalDate.of(year - 1, 12, 31)
            while (true) {
                date = date.with(TemporalAdjusters.next(day))
                if (date.getYear() != year) {
                    break
                }
                counts.put(day, counts.get(day) + 1)
            }
            if (counts.get(day) > maximum) {
                maximum = counts.get(day)
            }
        }
        days.stream().filter {counts.get(it) == maximum }
                .map {it.toString()[0] + it.toString().substring(1).toLowerCase() }
                .toArray()
    }
}
