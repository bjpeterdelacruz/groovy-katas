class ProductOfArrayItems {
    static def product(List<Integer> numbers) {
        if (numbers == null || numbers.isEmpty()) {
            return null
        }
        def product = 1
        numbers.forEach {number -> product *= number }
        product
    }
}
