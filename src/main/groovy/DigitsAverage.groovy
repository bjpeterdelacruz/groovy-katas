class DigitsAverage {
    static int digitsAverage(Integer number) {
        if (number < 10) {
            return number
        }
        def string = number.toString()
        while (true) {
            def result = ""
            for (def idx = 1; idx < string.length(); idx++) {
                def number1 = Integer.parseInt(string[idx - 1])
                def number2 = Integer.parseInt(string[idx])
                result += Math.round((number1 + number2) / 2)
            }
            if (result.length() == 1) {
                return Integer.parseInt(result)
            }
            string = result
        }
    }
}
