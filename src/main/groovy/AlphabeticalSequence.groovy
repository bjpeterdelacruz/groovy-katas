class AlphabeticalSequence {
    static def alphaSeq(String string) {
        def lowercase = "abcdefghijklmnopqrstuvwxyz"
        def results = []
        string.toLowerCase().each {
            def str = it * (lowercase.indexOf(it) + 1)
            results.add(str[0].toUpperCase() + str.substring(1))
        }
        results.sort()
        results.join(',')
    }
}
