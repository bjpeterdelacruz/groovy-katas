class SimpleTransposition {

    static def simpleTransposition(String text) {
        def row1 = []
        def row2 = []
        for (def idx = 0; idx < text.length(); idx++) {
            if (idx % 2 == 0) {
                row1.add(text[idx])
            }
            else {
                row2.add(text[idx])
            }
        }
        row1.join() + row2.join()
    }

}
