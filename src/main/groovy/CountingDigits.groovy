class CountingDigits {
    static def countSpecDigits(List<Integer> integers, List<Integer> digits) {
        def counts = [:]
        digits.each { counts.put(it, 0) }
        integers.each {
            def str = it.toString()
            digits.each {
                counts.put(it, counts.get(it) + str.count(it.toString()))
            }
        }
        def results = []
        digits.each { results.add([it, counts.get(it)]) }
        results
    }
}
